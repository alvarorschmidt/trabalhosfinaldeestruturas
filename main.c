#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define obter_tamanho_lista(lista) (sizeof((lista)) / sizeof((lista)[0]))

typedef struct retorno_busca {
    int resultado_busca;
    double tempo_busca;
} RetornoBusca;

clock_t obter_clockms();
RetornoBusca obter_tempo_execucao(int (*funcao_busca)(int, int[], int), int valor, int lista[], int tamanho_lista);
int busca_sequencial(int valor, int lista[], int tamanho_lista);
int busca_binaria_iterativa(int valor, int lista[], int tamanho_lista);
int busca_binaria_recursiva(int valor, int lista[], int tamanha_lista);
int busca_binaria(int valor, int lista[], int index_esquerdo, int index_direito);
void preencher_lista_randomicamente(int lista[], int tamanho_lista);
void merge(int lista[], int index_esquerdo, int index_meio, int index_direito);
void merge_sort(int lista[], int index_inicial, int index_final);
void print_lista(int lista[], int tamanho_lista);

int main() {
    int tamanho_listas[] = {10000, 50000, 100000, 200000};
    int valor_busca = 333;

    for(int i_lista = 0; i_lista < obter_tamanho_lista(tamanho_listas); i_lista++) {
        RetornoBusca retorno;
        int tamanho_lista = tamanho_listas[i_lista];

        int lista[tamanho_lista];
        preencher_lista_randomicamente(lista, tamanho_lista);

        printf("Buscando valor %d na lista de tamanho %d.\n", valor_busca, tamanho_lista);

        retorno = obter_tempo_execucao(busca_sequencial, valor_busca, lista, tamanho_lista);
        printf("Busca Sequencial retornou %d em um tempo de %10.4f\n\n", retorno.resultado_busca, retorno.tempo_busca);

        merge_sort(lista, 0, tamanho_lista - 1);

        retorno = obter_tempo_execucao(busca_binaria_iterativa, valor_busca, lista, tamanho_lista);
        printf("Busca Binaria Iterativa retornou %d em um tempo de %10.4f\n\n", retorno.resultado_busca, retorno.tempo_busca);

        retorno = obter_tempo_execucao(busca_binaria_recursiva, valor_busca, lista, tamanho_lista);
        printf("Busca Binaria Recursiva retornou %d em um tempo de %10.4f\n\n", retorno.resultado_busca, retorno.tempo_busca);

        printf("\n\n");
    }

    return 0;
}

clock_t obter_clockms() {
    return clock()/(CLOCKS_PER_SEC/1000);
}

RetornoBusca obter_tempo_execucao(int (*funcao_busca)(int, int[], int),
                            int valor,
                            int lista[],
                            int tamanho_lista){
    int resultado;
    clock_t inicio, fim;
    double tempo_execucao = 0;

    inicio = obter_clockms();
    resultado = funcao_busca(valor, lista, tamanho_lista);
    fim = obter_clockms();
    tempo_execucao = fim - inicio;

    return (RetornoBusca) {.resultado_busca = resultado, .tempo_busca = tempo_execucao};
}

int busca_sequencial(int valor, int lista[], int tamanho_lista) {
    int i;
    for(i = 0; i < tamanho_lista; i++) {
        if(lista[i] == valor)
            return 1;
    }
    return 0;
}

int busca_binaria(int valor, int lista[], int index_esquerdo, int index_direito) {
    if(index_direito >= index_esquerdo) {
        int index_meio = (index_esquerdo + index_direito) / 2;

        int valor_meio_lista = lista[index_meio];
        if(valor == valor_meio_lista)
            return 1;
        if(valor < valor_meio_lista)
            return busca_binaria(valor, lista, index_esquerdo, index_meio-1);

        return busca_binaria(valor, lista, index_meio + 1, index_direito);
    }

    return 0;
}

int busca_binaria_iterativa(int valor, int lista[], int tamanho_lista) {
    int index_esquerdo = 0;
    int index_direito = tamanho_lista - 1;

    while(index_esquerdo <= index_direito) {
        int index_meio = (index_esquerdo + index_direito)/2;

        int valor_meio_lista = lista[index_meio];
        if(valor == valor_meio_lista)
            return 1;
        else if(valor < valor_meio_lista)
            index_direito = index_meio - 1;
        else
            index_esquerdo = index_meio + 1;
    }

    return 0;
}

int busca_binaria_recursiva(int valor, int lista[], int tamanha_lista) {
    int index_esquerdo = 0;
    int index_direito = tamanha_lista - 1;

    return busca_binaria(valor, lista, index_esquerdo, index_direito);
}

void preencher_lista_randomicamente(int lista[], int tamanho_lista) {
    srand(time(NULL));
    for(int i = 0; i < tamanho_lista; ++i) {
        lista[i] = rand() % tamanho_lista;
    }
}

void merge(int lista[], int index_esquerdo, int index_meio, int index_direito){
    int i, j, k;
    int tamanho_esquerda = index_meio - index_esquerdo + 1;
    int tamanho_direira = index_direito - index_meio;

    int E[tamanho_esquerda], D[tamanho_direira];

    for (i = 0; i < tamanho_esquerda; i++)
        E[i] = lista[index_esquerdo + i];
    for (j = 0; j < tamanho_direira; j++)
        D[j] = lista[index_meio + 1+ j];

    i = 0;
    j = 0;
    k = index_esquerdo;
    while(i < tamanho_esquerda && j < tamanho_direira) {
        if (E[i] <= D[j]) {
            lista[k] = E[i];
            i++;
        }
        else {
            lista[k] = D[j];
            j++;
        }
        k++;
    }
    while (i < tamanho_esquerda) {
        lista[k] = E[i];
        i++;
        k++;
    }
    while (j < tamanho_direira) {
        lista[k] = D[j];
        j++;
        k++;
    }
}

void merge_sort(int lista[], int index_esquerdo, int index_direito){
    if (index_esquerdo < index_direito){
        int index_meio = index_esquerdo + (index_direito - index_esquerdo)/2;

        merge_sort(lista, index_esquerdo, index_meio);
        merge_sort(lista, index_meio + 1, index_direito);

        merge(lista, index_esquerdo, index_meio, index_direito);
    }
}

void print_lista(int lista[], int tamanho_lista) {
    for(int i = 0; i < tamanho_lista; ++i) {
        printf(" %d ", lista[i]);
    }
    printf("\n");
}
